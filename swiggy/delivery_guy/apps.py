from django.apps import AppConfig


class DeliveryGuyConfig(AppConfig):
    name = 'delivery_guy'

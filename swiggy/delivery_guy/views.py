from django.shortcuts import render
from django.contrib.gis.geos import Point
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from core.models import Order, OrderedItem, User, DeliveryGuyLocation

from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@login_required
def delivery_guy_dashboard(request, pk):
    orders = Order.objects.filter(delivery_guy=pk).all()
    completed_orders = []
    active_order = None
    for order in orders:
        if order.status.order_delivered == True:
            completed_orders.append(order)
        else:
            active_order = order
    return render(request, 'delivery_dashboard.html',
                  {'active_order': active_order, 'completed_orders': completed_orders, 'pk': pk})


@login_required
def delivery_order(request, pk):
    order = Order.objects.filter(id=pk).first()
    order_items = OrderedItem.objects.filter(order=pk).all()
    food_items = []
    for order_item in order_items:
        food_items.append(order_item.food)
    return render(request, 'order_detail.html', {'order':order, 'food_items':food_items})


@csrf_exempt
def order_status(request, pk):
    order = Order.objects.filter(id=pk).first()
    if 'delivery_guy_assigned' in request.POST.keys() and request.POST['delivery_guy_assigned']=='true':
        order.status.delivery_guy_assigned=True
    elif 'order_delivered' in request.POST.keys() and request.POST['order_delivered']=='true':
        order.status.order_delivered=True
    order.status.save()
    return JsonResponse({})


@csrf_exempt
def get_location(request, pk):
    if request.method == "POST":
        delivery_user = User.objects.filter(id=pk).first()
        delivery_guy = DeliveryGuyLocation.objects.filter(delivery_guy=delivery_user).first()
        if delivery_guy is None:
            delivery_guy = DeliveryGuyLocation(delivery_guy=delivery_user,location=Point(float(request.POST['lat']),float(request.POST['lng']), srid=4326))
        else:
            delivery_guy.location = Point(float(request.POST['lat']),float(request.POST['lng']), srid=4326)
        delivery_guy.save()
    return JsonResponse({})

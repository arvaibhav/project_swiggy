try{


$('#feedback-message').toggle(2500);

}
catch(e){
}


$('.all-food ul').click(
    function(e)
    {
        category_id= $(this).attr('category_id')
        if(category_id!='all')
        generateFoodItems(category_id,false)
        else
        allCategorySelectedDiv()
        swictchContainer(1);

    }
);

$('#choice-add-food-items .add-food').click(
    function(e)
    {
        swictchContainer(2);

    }
);

    function allCategorySelectedDiv( )
    {

        generateFoodItems(0,true)
        swictchContainer(1);
    }


// -----------------------------
//
var foodItems=[]



// later
function showFoodSelected(foodId){

return false;}


function generateFoodItems(category_id, allCategory)
{$('#all-food-items .container').empty()
    if(allCategory==true)
    {
        for(let i=0;i<foodItems.length;i++)
        {
          addFoodItem(foodItems[i].name,foodItems[i].cost,foodItems[i].preparing_time
            ,foodItems[i].img_path,foodItems[i].id)
        }
    }

    else{
        var  filteredFoodItems=[]
        for(let i=0;i<foodItems.length;i++)
        {


// temp
            for(let y=0;y<foodItems[i].category.length;y++)
            {
            if(foodItems[i].category[y].id==category_id)
            {
            filteredFoodItems.push(foodItems[i])

            }

            }

        }
        for(let i=0;i<filteredFoodItems.length;i++){
            addFoodItem(filteredFoodItems[i].name,filteredFoodItems[i].cost,
                filteredFoodItems[i].preparing_time
                ,filteredFoodItems[i].img_path,filteredFoodItems[i].id)
        }


}
}

function addFoodItem(foodName,foodCost,foodTime,foodImagePath,foodId)
{
  let foodItemMainDiv=  $('<div/>', {

        'class':'food-item',

    }).on('click', function(){
        showFoodSelected(foodId);
    })

    let foodImageDiv= $('<div/>', {

        'class':'food-image',

    }).append($('<img/>', {

        'src':foodImagePath,
        'alt':foodId


    }))

    let foodDetailDiv=$('<div/>', {

        'class':'food-detail'

    })
    let foodNameDiv= $('<input/>', {

        'class':'food-name',
        'value':foodName,
        "disabled":"disabled"
    })
//    .append($('<span/>', {
//
//
//        text:'Food: '
//    }))

    let foodCostDiv= $('<input/>', {

        'class':'food-cost',
        'value':foodCost,
        "disabled":"disabled"
    })
    let foodTimeDiv= $('<input/>', {

        'class':'food-preparing-time',
'value':foodTime,
        "disabled":"disabled"
    })




        let buttonContainer= $('<div/>', {

        'class':'button_container'
    })

     let editButton= $('<button/>', {

        'class':'edit_food_item food_item_change_button' ,
        text:'Edit'
    }).on('click', function(){
        editFoodItem(foodId,this);
    })

    let removeButton= $('<button/>', {

        'class':'remove_food_item food_item_change_button' ,
        text:'Remove'
    }).on('click', function(){
        removeFoodItem(foodId,this);
    })
       buttonContainer.append(editButton)
       buttonContainer.append(removeButton)




//    foodTimeDiv.html('<span >Time:</span>'+foodTime)
//    foodCostDiv.html('<span >Cost:</span>'+foodCost)
//    foodNameDiv.html('<span >Name:</span>'+foodName)


     foodDetailDiv.append(foodNameDiv)
    foodDetailDiv.append(foodCostDiv)
    foodDetailDiv.append(foodTimeDiv)



    foodItemMainDiv.append(foodImageDiv)
    foodItemMainDiv.append(foodDetailDiv)
     foodItemMainDiv.append(buttonContainer)
    foodItemMainDiv.appendTo('#all-food-items .container')


}
var imageFile;

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
        imageFile=reader
    }
}


function removeFoodItem(foodID,element){
var parentDiv= $(element).parent().parent()
$.ajax({
            url: '/dashboard/remove-food/',
            type: "POST",
            data: {
                'food_item_id': foodID,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken').val()
            },
            dataType: 'json',
            success: function (data) {
                if(data.result)
                {

                }
            }
        });

parentDiv.toggle(500)
}

function editFoodItem(foodID,element){
var parentDiv= $(element).parent().parent()
var editButton=(parentDiv.find('.edit_food_item'));
if(parentDiv.find('.food-name').attr("disabled")=="disabled")
{
parentDiv.find('.food-name').removeAttr('disabled');
parentDiv.find('.food-cost').removeAttr('disabled');
parentDiv.find('.food-preparing-time').removeAttr('disabled');
editButton.text('save')
return false;
}

//remove from database ,remove from array // referesh the array


parentDiv.find('.food-name').attr('disabled','disabled');
parentDiv.find('.food-cost').attr('disabled','disabled');
parentDiv.find('.food-preparing-time').attr('disabled','disabled');



           food_item_id =  foodID
            name =  parentDiv.find('.food-name').val().replace('Name:','')
            cost = parentDiv.find('.food-cost').val().replace('Cost:','')
//            food_desc =parentDiv.find('.food-name').text()
            preparing_time =parentDiv.find('.food-preparing-time').val().replace('Time:','')



    $.ajax({
            url: '/dashboard/edit-food/',
            type: "POST",
            data: {
                'food_item_id': foodID,
                'name': name,
                'cost': cost,
//                'food_desc': food_desc,
                'preparing_time': preparing_time,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken').val()
            },
            dataType: 'json',
            success: function (data) {
                if(data.result)
                {
editButton.text('edit')
alert('saved')
                }
            }
        });
}

getNotification();
function getNotification(){


}





function generateListOfNotification()
{
 $('.add-order-item').empty()
console.log(foodOrderItems)
var count=0;
for(let i=0;i<foodOrderItems.length;i++)
{
try{

    if(foodOrderItems[i].status.order_prepared== false)
    {

       let orderId=(foodOrderItems[i].ordereditem_set[0].order.id)
       let orderDate=(foodOrderItems[i].ordereditem_set[0].order_date_time)
        // foodname,cost
        var combined=""
for(let j=0;j<foodOrderItems[i].ordereditem_set .length;j++)
{   if(j==0)
    combined+=(foodOrderItems[i].ordereditem_set[j].food.name+"  "+foodOrderItems[i].ordereditem_set[j].quantity)
    else
    combined+=", "+(foodOrderItems[i].ordereditem_set[j].food.name+"  "+foodOrderItems[i].ordereditem_set[j].quantity)

}


let orderItemMainDiv=  $('<ul/>', {

    })
    let orderItemHead=  $('<li/>', {
    text:combined+" :id:"+orderId+" : "
    }).on('click', function(){
        getOrderInformation(foodOrderItems[i]);
    })
    let orderItembtn=  $('<button/>', {
        text:"Prepared",
        'orderID':orderId,
        'class':'order-prepared-btn'
        }).on('click', function(){
            setOrderPrepared(foodOrderItems[i],this);
        })

        orderItemMainDiv.append(orderItemHead)
        orderItemMainDiv.append(orderItembtn)
        orderItemMainDiv.appendTo('.add-order-item')



 count++;
    }

}
catch(e)
{
}}


changeTitle(count)
}



function getOrderInformation(orderItem)
{
      var mainDiv=$('<div/>', {
    'class':'order-detail-list-item'
})
    $('#ordered-item-data-container').empty()

let orderItemDetailDiv=  $('<div/>', {
    'class':'order-item-detail'
})
let orderItemDetailDivLabel=$('<label/>', {
    text:'Order Item'
})
//
let orderItemDetailListDiv=$('<ul/>', {
    'class':'nested',

})

let orderItemDetailListDivStart=$('<li/>', {


})

orderItemDetailDiv.append(orderItemDetailDivLabel)
orderItemDetailDiv.append(orderItemDetailListDiv)
var total=0
var totaltime=0
for(let i=0;i<orderItem.ordereditem_set.length;i++)
{
    let orderx=orderItem.ordereditem_set[i]
    total+=(orderx.food.cost*orderx.quantity)
    totaltime+=(orderx.food.preparing_time*orderx.quantity)
    // array
let orderItemDetailListDivHead=$('<label/>', {
    text:orderx.food.name,

})
// array element start
let orderItemDetailListSub=$('<ul/>', {


})
// array element detail
// var total cost
// 1.cost, qauntity, date
let orderItemDetailListSubItemCost=$('<li/>', {
     text:"Cost : "+orderx.food.cost

})
let orderItemDetailListSubItemQuant=$('<li/>', {
    text:"Quantity : "+orderx.quantity

})
let orderItemDetailListSubItemPTime=$('<li/>', {
    text:"Preparing Time : "+orderx.food.preparing_time+" minutes"

})


orderItemDetailListDivStart.append(orderItemDetailListDivHead)
orderItemDetailListDivStart.append(orderItemDetailListSub)
// list
orderItemDetailListSub.append(orderItemDetailListSubItemCost)
orderItemDetailListSub.append(orderItemDetailListSubItemQuant)
orderItemDetailListSub.append(orderItemDetailListSubItemPTime)


//


orderItemDetailListDiv.append(orderItemDetailListDivStart)
// list

}



let orderItemTotal=$('<div/>', {
    "class":"order-item-total",
    text:'Total Cost : ' +total

})
let orderItemTotalTime=$('<div/>', {
    "class":"order-item-total-time",
    text:'Time Required to Prepare : ' +totaltime+" minute"

})
var createdDate = orderItem.ordereditem_set[0].order.order_date_time;
createdDate = new Date(createdDate);
date = createdDate.toLocaleDateString();
time = createdDate.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
var hours = createdDate.getHours();
var minutes = createdDate.getMinutes();
var ampm = hours >= 12 ? 'pm' : 'am';
hours = hours % 12;
hours = hours ? hours : 12; // the hour '0' should be '12'
minutes = minutes < 10 ? '0'+minutes : minutes;
var strTime = hours + ':' + minutes + ' ' + ampm;


let orderItemDate=$('<div/>', {
    "class":"order-item-tdate",
    text:'Ordered At : ' +date+'  at  '+strTime

})




let orderItemButton=$('<button/>', {

    text:'Prepared',
    'orderID':orderItem.ordereditem_set[0].order.id,
    'class':'order-prepared-btn'

}).on('click', function(){
    setOrderPrepared(orderItem,this);
})
mainDiv.append(orderItemDetailDiv)
mainDiv.append(orderItemTotal)
mainDiv.append(orderItemTotalTime)
mainDiv.append(orderItemDate)
mainDiv.append(orderItemButton)
mainDiv.appendTo('#ordered-item-data-container')
swictchContainer(3)

}

function setOrderPrepared(orderItem,div)
{
// 1.send to server message
status_id=$(div).attr('orderid')


// 2.Do in try catch
// 3.if server respond true change the value of orderstatus to true in javascript

$.ajax({
            url: '/dashboard/status-prepared/'+restaurantId,
            type: "POST",
            data: {
                'status_id': status_id,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken').val()
            },
            dataType: 'json',
            success: function (data) {
                if(data.result)
                {

                }
            }
        });
// after remove try seal the prepared button and reresh from server
orderItem.status.order_prepared=true

if($(div).parent()[0].className==='')
$(div).parent().toggle(1000)
else
$(div).text('Status Updated').attr("disabled", true);
// and reget data from server
//


}
function changeTitle(notificationLength)
{
document.title ="("+ notificationLength+') Swiggy | DashBoard';
}




function swictchContainer(containerNumber)
{
// Order
// 1.Product Detail
// 2.Add Product
// 3. ordered item detail

let productDetailDiv=$('#all-food-items')
let addProductDiv=$('#add-food-items')
let orderedItemDetailDiv=$("#order-item-detail-container")

let allProdctActive=(productDetailDiv.css('display') != 'none')
let addProductActive= (addProductDiv.css('display') != 'none')
let orderedItemDetailActive  =(orderedItemDetailDiv.css('display') != 'none')

switch(containerNumber) {
    case 1:{
        addProductDiv.hide()
        orderedItemDetailDiv.hide()
        productDetailDiv.show()
    }

      break;
    case 2:{
        productDetailDiv.hide()
        orderedItemDetailDiv.hide()
        addProductDiv.show()
    }
        break;

    case 3:{
        productDetailDiv.hide()
        addProductDiv.hide()
        orderedItemDetailDiv.show()
    }
        break;

  }

}
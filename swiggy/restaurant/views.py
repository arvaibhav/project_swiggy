import base64
import io

import boto3
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect
from core.models import FoodItem, User, Restaurant, Category, Order, OrderStatus

from .forms import FoodItemForm
from .serializers import FoodItemSerializer, RestaurantOrderSerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def add_food_item(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            food_category_id = int(request.POST.get('food_category', None))
            food_name = request.POST.get('food_name', None)
            food_cost = request.POST.get('food_cost', None)
            food_desc = request.POST.get('food_desc', None)
            preparation_time = int(request.POST.get('food_time', None))
            restaurant = Restaurant.objects.filter(owner_id=request.user.id).first()

            if restaurant:
                food_item = FoodItem(name=food_name, cost=food_cost, food_desc=food_desc,
                                     preparing_time=preparation_time)
                food_item.save()
                category = Category.objects.filter(pk=food_category_id).first()
                food_item.category.add(category)
                restaurant.fooditem_set.add(food_item)
                restaurant.save()
                request.session['server_message'] = 'Added Food'
                img_path = upload_image_to_s3(request.FILES['myfile'], restaurant.pk, food_item.pk)
                food_item.img_path = img_path
                food_item.save()
                return redirect('restaurant_dashboard')
    request.session['server_message'] = 'Retry'
    return redirect('restaurant_dashboard')


def get_food_items_for_particular_category(request):
    if request.is_ajax:
        if request.user.is_authenticated:
            category_id = request.POST.get('category_id', None)
            food_items = FoodItem.objects.filter(pk=category_id)
    return JsonResponse({'result': False, 'food_items': food_items})


def make_order_prepared(request, status_id):
    if request.is_ajax:

        if request.user.is_authenticated:
            status_id = request.POST.get('status_id', None)
            order_status = OrderStatus.objects.filter(pk=status_id).first()
            if order_status:
                order_status.order_prepared = True
                order_status.save()
                return JsonResponse({'result': True})
    return JsonResponse({'result': False})


def edit_food_item(request):
    if request.is_ajax:

        if request.user.is_authenticated:
            food_item_id = request.POST.get('food_item_id', None)
            name = request.POST.get('name', None)
            cost = request.POST.get('cost', None)
            food_desc = request.POST.get('food_desc', None)
            preparing_time = request.POST.get('preparing_time', None)
            # img_path = request.POST.get('img_path', None)
            user = request.user
            food_item = user.restaurant.fooditem_set.get(pk=food_item_id)

            if food_item:
                food_item.name = name
                food_item.cost = cost
                # food_item.food_desc = food_desc
                food_item.preparing_time = preparing_time
                food_item.save()
                # food_item.img_path = img_path
                # image from ajax need to check if possible else use django forms
                return JsonResponse({'result': True})
    return JsonResponse({'result': False})


def remove_food_item(request):
    if request.is_ajax:
        if request.user.is_authenticated:
            food_item_id = request.POST.get('food_item_id', None)
            if food_item_id:
                user = request.user
                food_item = user.restaurant.fooditem_set.get(pk=food_item_id)
                if food_item:
                    food_item.delete()
                    return JsonResponse({'result': True})
    return JsonResponse({'result': False})


def restaurant_dashboard(request):
    if request.user.is_authenticated:
        if Restaurant.objects.filter(owner=request.user).exists():
            if 'server_message' not in request.session:
                request.session['server_message'] = None
            server_message = request.session['server_message']
            request.session['server_message'] = None
            return render(request, 'restaurant_dashboard.html', {'server_feedback': server_message})
    return redirect('login')


def upload_image_to_s3(image_file, restaurant_id, food_id):
    try:
        image_name = str(restaurant_id) + '_' + str(food_id) + '.jpg'
        s3 = boto3.resource('s3')
        s3.Bucket('mountblue-swigy').put_object(Key='images/' + image_name, Body=image_file, ACL='public-read')
        return 'https://mountblue-swigy.s3.ap-south-1.amazonaws.com/images/' + image_name
    except:

        return ""


def change_restaurant_name(request):
    if request.is_ajax:
        if request.user.is_authenticated:
            restaurant_name = request.POST.get('restaurant_name', None)
            if restaurant_name:
                request.user.restaurant.restaurant_name = restaurant_name
                request.user.save()
                return JsonResponse({'result': True})
    return JsonResponse({'result': False})


@api_view(['GET'])
def restaurant_food_items(request, pk):
    try:
        obj = FoodItem.objects.filter(restaurant_id=pk).all()
        serializer = FoodItemSerializer(obj, many=True)
        return Response(serializer.data)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)


# @csrf_exempt
@api_view(['GET'])
def pending_orders(request, pk):
    try:
        obj = Order.objects.filter(restaurant_id=pk).all()
        serializer = RestaurantOrderSerializer(obj, many=True)
        return Response(serializer.data)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

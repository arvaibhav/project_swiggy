from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views
from . import views


urlpatterns = [
    path('dashboard/', views.restaurant_dashboard, name='restaurant_dashboard'),
path('dashboard/add-food-item', views.add_food_item, name='add_food_item'),
    path('dashboard/food-items/<int:pk>', views.restaurant_food_items, name='restaurant_food_items'),
    path('dashboard/pending-order/<int:pk>', views.pending_orders, name='pending_orders'),
    path('dashboard/logout/', auth_views.LogoutView.as_view(), name='logout'),
path('dashboard/edit-food/', views.edit_food_item, name='edit_food_item'),
path('dashboard/remove-food/', views.remove_food_item, name='remove_food_item'),
path('dashboard/status-prepared/<int:status_id>', views.make_order_prepared, name='make_order_prepared'),



    ]
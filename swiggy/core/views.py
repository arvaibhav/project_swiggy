import boto3
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.gis.geos import Point
from .forms import SignUpForm, LoginForm,RestaurantRegisterForm
from .models import User, Restaurant, Location


# Create your views here.
def home_page(request):
    return render(request, 'homepage.html')


def signup(request):
    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password1']
        address = request.POST['address']
        email = request.POST['email']
        user_type = request.POST['user_type']
        mobile = request.POST['user_mobile']
        user = User(username=username, email=email, user_type=user_type, user_mobile=mobile, address=address)
        user.set_password(password)
        user.save()
        if user_type == 'restaurant':
            login(request, user)
            return redirect('register_restaurant')
        return redirect('home')
    else:
        sign_up_form = SignUpForm()
    return render(request, 'registration/signup.html', {'sign_up_form': sign_up_form})


def username_exists_in_users(username):
    return User.objects.filter(username=username).exists()


@csrf_exempt
def login_user(request):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            lat =float(12.9107931)
            lng =float(77.5963159)
            if 'lat' in request.POST:
                lat=float(request.POST['lat'])
            if 'lng' in request.POST:
                lng=float(request.POST['lng'])

            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                user = get_object_or_404(User, username=username)
                if user.user_type == 'customer':
                    location = Location.objects.filter(user = user).first()
                    if location is not None:
                        location.location = Point(lat, lng)
                    else:
                        location = Location(user=user, location=Point(lat, lng))
                    location.save()
                    return redirect('')
                    # return JsonResponse({'url':'restaurants/location'})
                elif user.user_type == 'restaurant':
                    location = Location.objects.filter(user = user).first()
                    if location is not None:
                        location.location = Point(lat, lng)
                    else:
                        location = Location(user=user, location=Point(lat, lng))
                    location.save()
                    return JsonResponse({'url':'/dashboard'})
                else:
                    return JsonResponse({'url':'/delivery/dashboard/{}'.format(user.id)})
            else:
                messages.error(request, 'invalid username or password')
                return render(request, 'registration/login.html', {'form': login_form})
    else:
        login_form = LoginForm()
        return render(request, 'registration/login.html', {'form': login_form})


def register_restaurant(request):
    if request.method == 'POST':

        restaurant_name = request.POST['restaurant_name']
        restaurantaddress =  request.POST['restaurantaddress']
        restaurant=Restaurant(restaurantaddress=restaurantaddress,restaurant_name=restaurant_name,owner=request.user)
        request.user.restaurant=(restaurant)
        img_path = upload_image_to_s3(request.FILES['myfile'], str(restaurant.pk)+"user", request.user.id)
        restaurant.img_path=img_path
        request.user.restaurant = (restaurant)
        request.user.save()
        restaurant.save()
        return  redirect('restaurant_dashboard')
    else:
        form = RestaurantRegisterForm()
        return render(request, 'restaurant_registeration.html', {'form': form})



def upload_image_to_s3(image_file, restaurant_id, food_id):
    try:
        image_name = str(restaurant_id) + '_' + str(food_id) + '.jpg'
        s3 = boto3.resource('s3')
        s3.Bucket('mountblue-swigy').put_object(Key='images/' + image_name, Body=image_file, ACL='public-read')
        return 'https://mountblue-swigy.s3.ap-south-1.amazonaws.com/images/' + image_name
    except:

        return ""
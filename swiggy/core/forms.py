from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User,Restaurant


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'user_mobile', 'user_type', 'password', 'address')
        labels = {'user_type': 'user_type'}


class RestaurantRegisterForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        fields = ('restaurant_name' , 'restaurantaddress',)



class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput)

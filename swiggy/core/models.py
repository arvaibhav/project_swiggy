from django.contrib.gis.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):
    user_types = (
        ('customer', 'CUSTOMER'),
        ('delivery_guy', 'DELIVERY_PERSON'),
        ('restaurant', 'RESTAURANT'),
    )
    user_type = models.CharField(max_length=50, choices=user_types, default='customer')
    user_mobile = PhoneNumberField(null=False, blank=False, unique=True)
    address = models.TextField(blank=True)


class Location(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user', blank=True, null=True)
    location = models.PointField()


class DeliveryGuyLocation(models.Model):
    delivery_guy = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    location = models.PointField()


class Restaurant(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE, related_name='restaurant', blank=True, null=True)
    restaurant_name = models.TextField(blank=True, null=True)
    restaurantaddress = models.CharField(max_length=50)
    img_path = models.URLField(blank=True, null=True)


class Category(models.Model):
    category_name = models.CharField(max_length=50)


class FoodItem(models.Model):
    name = models.CharField(max_length=100)
    cost = models.IntegerField()
    food_desc = models.TextField(max_length=100)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, blank=True, null=True)
    preparing_time = models.IntegerField()
    img_path = models.URLField()
    category = models.ManyToManyField(Category)


class OrderStatus(models.Model):
    order_prepared = models.BooleanField(default=True)
    delivery_guy_assigned = models.BooleanField(default=True)
    order_delivered = models.BooleanField(default=False)


class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customer')
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    delivery_guy = models.ForeignKey(User, on_delete=models.CASCADE, related_name='delivery_guy', null=True)
    order_date_time = models.DateTimeField(default=timezone.now)
    status = models.OneToOneField(OrderStatus, on_delete=models.CASCADE, blank=True, null=True)
    total_cost = models.FloatField()


class OrderedItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, blank=True, null=True)
    food = models.ForeignKey(FoodItem, on_delete=models.CASCADE, blank=True, null=True)
    quantity = models.IntegerField()

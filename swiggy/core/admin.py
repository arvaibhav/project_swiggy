from django.contrib import admin
from django.db.models.base import ModelBase
from . import models


admin.site.register(models.User)
admin.site.register(models.Restaurant)
admin.site.register(models.Order)
admin.site.register(models.OrderStatus)
admin.site.register(models.DeliveryGuyLocation)
admin.site.register(models.Location)
admin.site.register(models.OrderedItem)
admin.site.register(models.FoodItem)
admin.site.register(models.Category)

from django.urls import path,include
from . import views
urlpatterns=[path('location/',views.location,name='location'),
path('restaurant/<int:pk>',views.restaurant,name='restaurant'),
path('checkout',views.checkout,name='checkout'),
path('status',views.status,name='status'),
path('locate',views.locate,name='locate'),
path('orderstatus/<int:pk>',views.order_status,name='orderstatus')
]
